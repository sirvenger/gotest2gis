package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"sync"
	"sync/atomic"
)

var totalUpdated int32
var totalDeleted int32
var fileReportName = "report.txt"

type job func(products map[string]Product, record []string, mu *sync.Mutex, wg *sync.WaitGroup, file *os.File)

type Product struct {
	name        string
	description string
	price       string
}

func incTotalUpdate() {
	atomic.AddInt32(&totalUpdated, 1)
}

func incTotalDeleted() {
	atomic.AddInt32(&totalDeleted, 1)
}

func writeToFile(message string, record []string, f *os.File, mu *sync.Mutex) {
	mu.Lock()
	defer mu.Unlock()
	_, err := fmt.Fprintln(f, message, record)
	if err != nil {
		panic(err)
	}
}

func findChanges(products map[string]Product, record []string, mu *sync.Mutex, wg *sync.WaitGroup, file *os.File) {
	defer wg.Done()
	key := record[0]
	mu.Lock()
	product, ok := products[key]
	mu.Unlock()
	if ok {
		if product.description != record[1] || product.price != record[2] {
			writeToFile("Товар обновлен: ", record, file, mu)
			incTotalUpdate()
		}
		mu.Lock()
		delete(products, record[0])
		mu.Unlock()
	} else {
		writeToFile("Товар удален: ", record, file, mu)
		incTotalDeleted()
	}
	runtime.Gosched()
}

func formMapProduct(products map[string]Product, record []string, mu *sync.Mutex, wg *sync.WaitGroup, file *os.File) {
	defer wg.Done()
	mu.Lock()
	products[record[0]] = Product{
		name:        record[0],
		description: record[1],
		price:       record[2],
	}
	mu.Unlock()
	runtime.Gosched()
}

func readFromFileCsvWithJob(filepath string, job job, products map[string]Product, mu *sync.Mutex, wg *sync.WaitGroup, fileOut *os.File) {
	csvFile, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.Comma = ';'

	header := []string{}
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if header == nil {
			header = record
		} else {
			wg.Add(1)
			go job(products, record, mu, wg, fileOut)
		}
	}

	wg.Wait()
}

func main() {
	if !(len(os.Args) == 4) {
		panic("usage go run main.go . [-f] fileNew fileOld")
	}
	fileOldProduct := os.Args[2]
	fileNewProduct := os.Args[3]

	mu := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	products := make(map[string]Product, 1000)

	fileReport, err := os.Create(fileReportName)
	if err != nil {
		fmt.Println(err)
	}
	fileReport.Truncate(0)

	readFromFileCsvWithJob(fileNewProduct, job(formMapProduct), products, mu, wg, fileReport)
	readFromFileCsvWithJob(fileOldProduct, job(findChanges), products, mu, wg, fileReport)

	for _, record := range products {
		writeToFile("Товар добавлен: ", []string{record.name, record.description, record.price}, fileReport, mu)
	}

	fmt.Println("Добавлено новых товаров: ", len(products))
	fmt.Println("Обновлено товаров: ", totalUpdated)
	fmt.Println("Удалено товаров: ", totalDeleted)

	fileReport.Close()
}
